###################################################################################################
# This is a compasion of branch zach-soc-bytecode-in-traceback in
# in branch zach-soc-bytecode-in-traceback 0cb1df1edd86986d5d7a3ecf607fe78af03d62a0
# with branch feature/soc-bytecode-in-traceback-reduced 9a36861ef6ff812684980a3d52a6bccc8ff4e727
#
# The only difference is to limiting the bytecode-offset to just before calls.
#
$ cat ./bench-compare.sh
#!/bin/bash
cd /src/external-vcs/gitlab/bench-compare.el/scripts/
./compare-benches.el -runs 1 -s bubble /src/external-vcs/gitlab/zach-emacs-reduced/src/emacs /src/external-vcs/savannah/emacs/src/emacs
$ ./bench-compare.sh
Loading /etc/emacs/site-start.d/00debian.el (source)...
Loading /etc/emacs/site-start.d/50autoconf.el (source)...
Loading /etc/emacs/site-start.d/50cmake-data.el (source)...
Loading /etc/emacs/site-start.d/50devhelp.el (source)...
Loading /etc/emacs/site-start.d/50dictionaries-common.el (source)...
Loading debian-ispell...
Loading /var/cache/dictionaries-common/emacsen-ispell-default.el (source)...
Loading /var/cache/dictionaries-common/emacsen-ispell-dicts.el (source)...
Loading /etc/emacs/site-start.d/50latexmk.el (source)...
Name for build at /src/external-vcs/gitlab/zach-emacs-reduced/src/emacs: reduced
Name for build at /src/external-vcs/savannah/emacs/src/emacs: zach
Running 1 benchmark iteration on 2 executables
Results will be printed after all benchmarks have run
reduced: Compiling and Loading benchmarks...
zach: Compiling and Loading benchmarks...
zach: Iteration number: 1
zach: Running bubble-no-cons...
reduced: Iteration number: 1
reduced: Running bubble-no-cons...
zach: Running bubble...
reduced: Running bubble...
zach: benchmarks finished
reduced: benchmarks finished
Results:

* reduced

  | test           | non-gc avg (s) | gc avg (s) | gcs avg | tot avg (s) | tot avg err (s) |
  |----------------+----------------+------------+---------+-------------+-----------------|
  | bubble-no-cons |          13.22 |       0.06 |       4 |       13.28 | -nan            |
  | bubble         |           4.99 |       6.94 |     506 |       11.94 | -nan            |
  |----------------+----------------+------------+---------+-------------+-----------------|
  | total          |          18.21 |       7.00 |     510 |       25.22 | -nan            |

* zach

  | test           | non-gc avg (s) | gc avg (s) | gcs avg | tot avg (s) | tot avg err (s) |
  |----------------+----------------+------------+---------+-------------+-----------------|
  | bubble-no-cons |          12.27 |       0.06 |       4 |       12.33 | -nan            |
  | bubble         |           4.91 |       6.91 |     505 |       11.83 | -nan            |
  |----------------+----------------+------------+---------+-------------+-----------------|
  | total          |          17.18 |       6.97 |     509 |       24.15 | -nan            |

* Difference (reduced - zach)
  | test           | non-gc avg | gc avg | gcs avg | tot avg | tot avg err |
  |----------------+------------+--------+---------+---------+-------------|
  | bubble-no-cons |       0.95 |   0.00 |    0.00 |    0.95 |        0.00 |
  | bubble         |       0.08 |   0.03 |    1.00 |    0.11 |        0.00 |
  |----------------+------------+--------+---------+---------+-------------|
  | total          |       1.03 |   0.03 |    1.00 |    1.07 |        0.00 |

* Ratio (reduced / zach)
  | test           | non-gc avg | gc avg | gcs avg | tot avg | tot avg err |
  |----------------+------------+--------+---------+---------+-------------|
  | bubble-no-cons |     107.7% | 100.0% |  100.0% |  107.7% |             |
  | bubble         |     101.6% | 100.4% |  100.0% |  100.9% |             |
  |----------------+------------+--------+---------+---------+-------------|
  | total          |     106.0% | 100.4% |  100.0% |  104.4% |             |


Done
$ ./bench-compare.sh
Loading /etc/emacs/site-start.d/00debian.el (source)...
Loading /etc/emacs/site-start.d/50autoconf.el (source)...
Loading /etc/emacs/site-start.d/50cmake-data.el (source)...
Loading /etc/emacs/site-start.d/50devhelp.el (source)...
Loading /etc/emacs/site-start.d/50dictionaries-common.el (source)...
Loading debian-ispell...
Loading /var/cache/dictionaries-common/emacsen-ispell-default.el (source)...
Loading /var/cache/dictionaries-common/emacsen-ispell-dicts.el (source)...
Loading /etc/emacs/site-start.d/50latexmk.el (source)...
Name for build at /src/external-vcs/gitlab/zach-emacs-reduced/src/emacs: reduced
Name for build at /src/external-vcs/savannah/emacs/src/emacs: zach
Running 1 benchmark iteration on 2 executables
Results will be printed after all benchmarks have run
reduced: Compiling and Loading benchmarks...
zach: Compiling and Loading benchmarks...
zach: Iteration number: 1
reduced: Iteration number: 1
zach: Running bubble-no-cons...
reduced: Running bubble-no-cons...
zach: Running bubble...
reduced: Running bubble...
zach: benchmarks finished
reduced: benchmarks finished
Results:

* reduced

  | test           | non-gc avg (s) | gc avg (s) | gcs avg | tot avg (s) | tot avg err (s) |
  |----------------+----------------+------------+---------+-------------+-----------------|
  | bubble-no-cons |          13.23 |       0.05 |       4 |       13.28 | -nan            |
  | bubble         |           5.04 |       6.93 |     511 |       11.97 | -nan            |
  |----------------+----------------+------------+---------+-------------+-----------------|
  | total          |          18.27 |       6.98 |     515 |       25.25 | -nan            |

* zach

  | test           | non-gc avg (s) | gc avg (s) | gcs avg | tot avg (s) | tot avg err (s) |
  |----------------+----------------+------------+---------+-------------+-----------------|
  | bubble-no-cons |          12.25 |       0.06 |       4 |       12.31 | -nan            |
  | bubble         |           4.90 |       6.81 |     490 |       11.71 | -nan            |
  |----------------+----------------+------------+---------+-------------+-----------------|
  | total          |          17.15 |       6.87 |     494 |       24.01 | -nan            |

* Difference (reduced - zach)
  | test           | non-gc avg | gc avg | gcs avg | tot avg | tot avg err |
  |----------------+------------+--------+---------+---------+-------------|
  | bubble-no-cons |       0.98 |  -0.01 |    0.00 |    0.97 |        0.00 |
  | bubble         |       0.14 |   0.12 |   21.00 |    0.26 |        0.00 |
  |----------------+------------+--------+---------+---------+-------------|
  | total          |       1.12 |   0.11 |   21.00 |    1.24 |        0.00 |

* Ratio (reduced / zach)
  | test           | non-gc avg | gc avg | gcs avg | tot avg | tot avg err |
  |----------------+------------+--------+---------+---------+-------------|
  | bubble-no-cons |     108.0% |  83.3% |  100.0% |  107.9% |             |
  | bubble         |     102.9% | 101.8% |  100.0% |  102.2% |             |
  |----------------+------------+--------+---------+---------+-------------|
  | total          |     106.5% | 101.6% |  100.0% |  105.2% |             |


Done
