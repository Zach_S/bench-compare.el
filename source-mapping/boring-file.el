;;; boring-file.el ---  -*- lexical-binding: t; -*-
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Dummy file for testing source-map.el. Contains a variety of data type
;;  literals to test the different source-map structs.
;;
;;; Code:

(defun boring-function (boring-arg1 boring-arg2)
  (list boring-arg1 boring-arg2
        #s(boring-record boring-symbol)
        #("boring propertized string" 0 6 (boring t))
        '#:boring-uninterned-symbol
        [boring vector]
        '(boring improper . list)
        #s(hash-table size 65
                      test eql
                      rehash-size 1.5
                      rehash-threshold 0.8125
                      data (boring t interesting nil))))

(defvar boring-char-table
  #^[boring nil boring boring boring
            boring boring boring boring boring boring boring boring boring
            boring boring boring boring boring boring boring boring boring
            boring boring boring boring boring boring boring boring boring
            boring boring boring boring boring boring boring boring boring
            boring boring boring boring boring boring boring boring boring
            boring boring boring boring boring boring boring boring boring
            boring boring boring boring boring boring boring boring boring])

(defvar boring-function-byte-code
  #[514 "\300\301\302\303\304\305\257\207"
    [#s(boring-record boring-symbol)
     #("boring propertized string" 0 6 (boring t))
     #:boring-uninterned-symbol
     [boring vector]
     (boring improper . list)
     #s(hash-table size 65 test
        eql rehash-size 1.5 rehash-threshold 0.8125 data (boring t interesting nil))]
    10
    ("/home/zach/.repos/bench-compare.el/source-mapping/boring-file.elc" . 87)])

(boring-function boring-char-table boring-function-byte-code)

(provide 'boring-file)
;;; boring-file.el ends here
