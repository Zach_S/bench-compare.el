;;; source-map-tests.el --- tests for source-map.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2020 Zach Shaftel
;;
;; Maintainer: Zach Shaftel <zshaftel@gmail.com>
;; Created: July 13, 2020
;; Modified: July 13, 2020
;; Version: 0.0.1
;; Keywords:
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  tests for source-map.el
;;  TODO way more tests
;;
;;; Code:

(eval-when-compile (require 'pcase))
(require 'ert)
(require 'source-map (expand-file-name "source-map"))

(defvar source-map-test-expressions
  `(("just a string"
     . #s(source-map-string
          "just a string" "just a string"
          1 14 "\"just a string\""))
    (#s(hash-table size 65 test eq rehash-size 1.5 rehash-threshold 0.8125 data ())
       . #s(source-map-hash-table
            #s(hash-table size 65 test eq rehash-size 1.5 rehash-threshold 0.8125 data ())
            #s(hash-table size 65 test eq rehash-size 1.5 rehash-threshold 0.8125 data ())
            1 79 "#s(hash-table size 65 test eq rehash-size 1.5 rehash-threshold 0.8125 data ())"))
    (([vector in] a dotted . list)
     . #s(source-map-list
          ([vector in] a dotted . list)
          (#s(source-map-vector
              [vector in]
              [#s(source-map-symbol vector vector 3 9 "vector")
                 #s(source-map-symbol in in 10 12 "in")]
              2 13 "[vector in]")
             #s(source-map-symbol a a 14 15 "a")
             #s(source-map-symbol dotted dotted 16 22 "dotted")
             . #s(source-map-symbol list list 24 29 "list"))
          1 30 "([vector in] a dotted . list)"))))

(defvar source-map-test-file
  (expand-file-name "boring-file.el"))

(ert-deftest source-map-read-behavior ()
  (with-temp-buffer
    (should-error (source-map-read (current-buffer)) :type '(end-of-file))))

(ert-deftest source-map-read-equivalence ()
  (with-temp-buffer
    (let ((standard-input (current-buffer)))
      (pcase-dolist (`(,sexp . ,obj) source-map-test-expressions)
        (save-excursion (prin1 sexp standard-input))
        (pcase-let* (((and srcobj (cl-struct source-map-expression
                                             (sexp srcsexp)
                                             (members srcmembers)))
                      (source-map-read)))
          ;; FIXME this test would still fail if it's a list containing a
          ;; hash-table
          (or (source-map-byte-code-p srcobj)
              (source-map-hash-table-p srcobj)
              (should (equal srcobj obj)))
          (or (consp sexp) (vectorp sexp)
              (should (eq srcsexp srcmembers))))
        (erase-buffer)))))

(ert-deftest source-map-file ()
  (when (file-exists-p source-map-test-file)
    (source-map-file source-map-test-file)))

(provide 'source-map-tests)
;;; source-map-tests.el ends here
