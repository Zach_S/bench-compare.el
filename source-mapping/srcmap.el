;; Skeletal (read) which stores offset and S-exps.

;; Ideas for this code (if not some code directly) come from edebug.el
(defconst test-data
  '(
    "\"A srcmap structure\""
    "1000"
    "nil"
    "t"
    "(+  1    2)"
    "( + a (* b c))"
    "( a . b ) "
    "(c . d)"
    "(defun five() 5)"
    )
  "Sample Sexps we can use for testing"
  )

(cl-defstruct srcmap
  "A Elisp source text mapping structure. With this we can map
  the start and end offsets and its string representation
  with its corresponding read() S-expression.

  Each non-atomic S-expression will has a list
  of its children with srcmap entries for those corresponding
  S-expressions."
  (start  -1)  ;; Start offset
  (end    -1)  ;; end offset
  (sexpr nil)  ;; the S-expression, if class == lparen
  (str   nil)  ;; string representation as seen in source
  (class nil)  ;; symbol class
  (children nil)  ;; list of srcmaps if class == lparen
  )

(defun test-srcmap-read-sexpr()
  "Testing (srcmap-read-sexpr)"
  (loop for sexpr in test-data
        do
        (with-temp-buffer

          ;; By giving a name to the buffer, we can see what's going
          ;; on during debugging. This line could be removed without
          ;; changing the behavior other than abiility to debug.
          (rename-buffer "*foo*")

          (insert (format "%s" sexpr))
          (beginning-of-buffer)
          (message "%S" (srcmap-read-sexpr)))))

(defconst srcmap-read-syntax-table
  "Lookup table for significant characters indicating the class of the
  token that follows.  This is not a \"real\" syntax table."
  (let ((table (make-char-table 'syntax-table 'symbol))
	(i 0))
    (while (< i ?!)
      (aset table i 'space)
      (setq i (1+ i)))
    (aset table ?\( 'lparen)
    (aset table ?\) 'rparen)
    (aset table ?\' 'quote)
    (aset table ?\` 'backquote)
    (aset table ?\, 'comma)
    (aset table ?\" 'string)
    (aset table ?\? 'char)
    (aset table ?\[ 'lbracket)
    (aset table ?\] 'rbracket)
    (aset table ?\. 'dot)
    (aset table ?\# 'hash)
    ;; We treat numbers as symbols, because of confusion with -, -1, and 1-.
    ;; We don't care about any other chars since they won't be seen.
    table))

(defun srcmap-skip-whitespace ()
  "Leave point before the next token of the current buffer,
   skipping white space and comments."
  (skip-chars-forward " \t\r\n\f")
  (while (= (following-char) ?\;)
    (skip-chars-forward "^\n")  ; skip the comment
    (skip-chars-forward " \t\r\n\f")))

(defun srcmap-read-sexpr (&optional buffer)
  "Read one S-expression from the `buffer' (or the current buffer) starting at
  that buffer's point.

  Leave point immediately after it.  An S-expression can be a list or an atom.
  An atom is a symbol (or number), character, string, or vector.
  This works for reading anything legitimate, but it
  is gummed up by parser inconsistencies (bugs?)

  Returns a srcmap for what is read.

  For special characters we need to care about:
     lparen, rparen, dot, quote, backquote, comma, string, char, vector,
     and symbol.
  "
  (with-current-buffer (or buffer (current-buffer))
    (srcmap-skip-whitespace)
    (let* (
	   (start (point))
	   (end start)
	   (class)
	   (str "")
	   (sexpr)
	   (children nil)
	   )

      ;; Check for two-character symbols that start with ".".
      (if (and (eq (following-char) ?.)
	       (save-excursion
		 ;; FIXME we should 1+ if the below "or" succeeds)
		 (setq end (1+ end))
		 (forward-char 1)
		 (or (and (eq (aref srcmap-read-syntax-table (following-char))
			      'symbol)
			  (not (= (following-char) ?\;)))
		     (memq (following-char) '(?\, ?\.)))))
	  (setq class 'symbol)
	;; else
	(setq class (aref srcmap-read-syntax-table (following-char)))
	)
    (cond
     ((eq class 'lparen)
      ;; Note: the 'lparen class is the same thing as a non-atomic or more
      ;; general s-expression class.
      ;; Think about whether we want to change the class name.
      (save-excursion
        (setq sexpr (read (current-buffer)))
        (setq end (point))
        (setq str (buffer-substring start end))
        )
      (forward-char 1)
      (setq children
            (loop while (<= (point) (1- end))
                  collect (srcmap-read-sexpr)))
      )
     ((memq class '(dot rparen backquote comma))
      ;; We can't do a (read) on these kinds of symbols. But since
      ;; they are canonical, i.e. can't change textual representation,
      ;; and since we know the length of these symbols which is 1, we
      ;; will use this information to advance the pointer and fill in
      ;; the :str and :end fields.
      (forward-char 1)
      (setq end (point))
      (setq str (buffer-substring start end))
      )
     (t
      (setq sexpr (read (current-buffer)))
      (setq end (point))
      (setq str (buffer-substring start end))
      ))
    (make-srcmap
     :start start
     :end end
     :sexpr sexpr
     :str str
     :class class
     :children children
     ))
    ))

;; Not used - is a copy of edebug-read-sexp which was the inspiration
;; for srcmap-read-sexpr
(defun srcmap-read-sexpr-orig (start-offset)
  ;; read one sexp from the current buffer starting at point.
  ;; leave point immediately after it.  a sexp can be a list or atom.
  ;; an atom is a symbol (or number), character, string, or vector.
  ;; this works for reading anything legitimate, but it
  ;; is gummed up by parser inconsistencies (bugs?)
  (let ((class (srcmap-next-token-class)))
    (cond
     ;; read goes one too far if a (possibly quoted) string or symbol
     ;; is immediately followed by non-whitespace.
     ((eq class 'symbol) (read (current-buffer)))
     ((eq class 'string) (read (current-buffer)))
     ((eq class 'quote) (forward-char 1)
      (list 'quote (srcmap-read-sexp)))
     ((eq class 'backquote) (forward-char 1)
      (list '\` (srcmap-read-sexp)))
     ((eq class 'comma) (forward-char 1)
      (list '\, (srcmap-read-sexp)))
     (t ; anything else, just read it.
      (read (current-buffer))))))

;; Todo: write macro or function to create or wrap common
;; Lisp functions that might be needed to access the source map
;; in the process of compilation
(defun srcmap-car(source-map)
  (car (srcmap-sexpr source-map)))
