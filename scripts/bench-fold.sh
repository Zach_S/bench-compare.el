#!/bin/bash
MODIFIED_CODE_DIR="/src/external-vcs/gitlab/zach-emacs"
BASELINE_CODE_DIR="/src/external-vcs/gitlab/zach-emacs-baseline"

# Add Flamegraph to paths
export PATH="/src/external-vcs/github/FlameGraph":$PATH

(
for dir in $MODIFIED_CODE_DIR $BASELINE_CODE_DIR; do
    cd $dir
    perf script | sed -e "s:$dir::" > ${dir}/out.stacks
    stackcollapse-perf.pl out.stacks > ${dir}/out.folded
done
)

difffolded.pl -n ${MODIFIED_CODE_DIR}/out.folded $BASELINE_CODE_DIR/out.folded | flamegraph.pl > diff2.svg
