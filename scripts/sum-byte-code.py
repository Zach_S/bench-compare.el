#!/usr/bin/env python
import sys
sum = 0
for line in open(sys.argv[1], "r").readlines():
    sum += int(line.split()[3])
print(sum)
