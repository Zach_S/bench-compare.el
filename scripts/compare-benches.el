#!/usr/bin/emacs --script
;;; compare-benches.el --- Run concurrent Emacs benchmarks -*- lexical-binding: t; -*-

;;; Commentary:

;;; TODO:

;; - Support noninteractive use
;; - Support collecting data from other tests than just elisp-benchmarks,
;;   eg. "make check"
;; - Come up with a better way to save the extra files than littering
;;   the extras dir
;; - Clean up global variables
;; - Find a better way to parse args (maybe via `command-line-functions'?)
;; - Ask before creating new directories

;;; Usage:

;;   ./compare-benches.el -runs 5 /path/to/emacs /path/to/other/emacs
;; For more detailed information:
;;   ./compare-benches.el -h
;; Or read `help-message' below

;;; Code:

(require 'org)
(require 'pcase)
(require 'subr-x)

(declare-function org-link-make-string "ol" (link &optional description))

(defun pretty-file-name (filename &optional dir)
  (if (file-in-directory-p filename (or dir default-directory))
      (concat (unless dir "./") (file-relative-name filename dir))
    (abbreviate-file-name filename)))

(defvar this-file
  (pretty-file-name
   (string-remove-suffix
    "c"
    (or (cadr (member "-scriptload" command-line-args))
        load-file-name
        buffer-file-name
        "./compare-benches.el"))))

(unless noninteractive
  (error "This file must be executed as a script
Run '%s -h' from the shell for information"
         this-file))

(defvar elpa-url
  (or (and (boundp 'package-archives)
           (cdr (assoc "gnu" package-archives)))
      "https://elpa.gnu.org/packages/"))

(defvar elisp-benchmarks-name "elisp-benchmarks")

(defvar elisp-benchmarks-version "1.6")

(defvar elisp-benchmarks-directory
  (format "../%s-%s/"
          elisp-benchmarks-name
          elisp-benchmarks-version))

(defvar elisp-benchmarks-file-path
  (concat elisp-benchmarks-directory elisp-benchmarks-name ".el"))

(defvar elisp-benchmarks-base-url
  (concat (file-name-as-directory elpa-url) elisp-benchmarks-name))

(defvar benchmark-runs 3)

(defvar benchmark-selector nil)

(defconst help-message
  (format
   "\
%1$s [OPTIONS] PATH-TO-EMACS...

Run a set of benchmarks on one or more emacs executables
simultaneously.

The benchmarks are from %3$s.html.

Options:

-runs      N       - Run the set of benchmarks N times (default 3)
-o         FILE    - Where the benchmark results go (just stderr if omitted)
-s         REGEXP  - Elisp regexp that selects which benchmarks to run
-extra-dir DIR     - Directory where perf and flamegraph extras go;
                       defaults to the directory of the -o arg
-no-extra          - Don't generate extra data via perf and flamegraph
-debug             - Enable `debug-on-error'
-h                 - Display this message


Optional environment variables:

PERF_BIN           - path to perf
                       can be omitted if `executable-find' will find it
PERF_FILTER        - grep regexp to filter the stackcollapse-perf
                       output, which is used to generate the flame graphs

See http://www.brendangregg.com/flamegraphs.html for more
information about flame graphs.

This script uses a slightly modified version of %2$s.el, which is
in the parent directory (it omits the \"* Results\" line, and
prints the table to stdout instead of stderr).

Example usage:

 %1$s -no-extra -o results.org \\
 -s \"flet\\\\|pcase\" -runs 5 \\
 /first/path/to/emacs /second/path/to/emacs

This runs just the \"flet\" and \"pcase\" benchmarks 5 times on
two executables, prints the results and writes them to
./results.org.

 PERF_BIN=/path/to/perf \\
 PERF_FILTER=Ffuncall \\
 %1$s -o ./results.org -extra-dir ./extras \\
 /first/path/to/emacs /second/path/to/emacs

This runs the executables via 'perf record', then collapses the
output from 'perf script', filters it with 'grep Ffuncall',
generates flame graphs from that, and saves the output and graphs
in the ./extras directory. The ./results.org file contains the
results of the benchmarks and links to those extra files."
   this-file
   elisp-benchmarks-name
   elisp-benchmarks-base-url))

(defvar percentage-table-formula-format
  (let ((print-quoted t))
    (prin1-to-string
     ''(let* ((d (string-to-number %2$s))
              q)
         (if (or (zerop d)
                 (zerop (setq q (/ (string-to-number %1$s) d))))
             ""
           (format "%%.1f%%%%" (* 100 q)))))))

(defvar table-top-row
  "| test | non-gc avg | gc avg | gcs avg | tot avg | tot avg err |")

(defvar table-sep "|-+-+-+-+-+-|")

(defvar table-bottom-row "| total | | | | | |")

(defun selected-benches (benches-dir)
  (delq nil (mapcar (lambda (file)
                      (when (or (null benchmark-selector)
                                (string-match-p benchmark-selector file))
                        (file-name-base file)))
                    (directory-files benches-dir :full "\\.el\\'"))))

(defun get-table-skeleton (bench-files)
  (concat table-top-row "\n" table-sep "\n"
          (mapconcat (lambda (file)
                       (format "| %s | | | | | |" file))
                     bench-files "\n")
          "\n" table-sep "\n" table-bottom-row))

(defsubst remote-table-ref (name)
  (format "remote(%s,@@#$$#)" name))

(defun insert-row-formula (tablerows &rest parts)
  (let ((oldp (point)))
    (apply #'insert (format "#+TBLFM: @2$2..@%d$6=" (+ tablerows 2)) parts)
    (put-text-property oldp (point) 'dont-print-me t))
  (insert "\n"))

(define-error 'finished "Done")

(defun finish (&rest data)
  (signal 'finished (unless (equal data '(:silently))
                      (or data '("Exiting")))))

(defun simple-proc-sentinel (proc string)
  (pcase string
    ("deleted\n")
    ("finished\n"
     (when-let (rep (process-get proc :report-finished))
       (if (stringp rep)
           (message "%s" rep)
         (message "Process %s is done" (process-name proc)))))
    (_ (error "In process %s: %s" (process-name proc)
              (let ((buf (process-buffer proc)))
                (if (buffer-live-p buf)
                    (concat string "\nCollected output:\n"
                            (with-current-buffer buf (buffer-string)))
                  string))))))

(defvar results-directory nil
  "The directory in which the benchmark results will go.
Corresponds to the directory of the file specified with the -o
flag.")

(defvar extra-data-directory nil
  "The directory in which stack information and flame graphs will
go. Defaults to `results-directory'.")

(defvar perf-executable
  (or (getenv "PERF_BIN") (executable-find "perf")))

(defvar stackcollapse-perf-executable
  (expand-file-name "stackcollapse-perf.pl" default-directory))

(defvar flamegraph-executable
  (expand-file-name "flamegraph.pl" default-directory))

(defvar difffolded-executable
  (expand-file-name "difffolded.pl" default-directory))

(defun get-benchmark-command (emacs-exe perf-outfile)
  ;; FIXME multiple processes may try to compile the benchmarks at the same time
  (let* ((form (format "(elisp-benchmarks-run %S t %s)"
                       benchmark-selector benchmark-runs))
         (ec (list emacs-exe "-Q" "--batch" "-l"
                   elisp-benchmarks-file-path "--eval" form)))
    (if (null perf-outfile)
        ec
      `(,perf-executable "record" "-m" "16" "-g"
                         "-o" ,perf-outfile ,@ec))))

(defun extra-file (name extension &optional delete)
  "Create a temp file NAME with EXTENSION in `extra-data-directory'.
A blank file is created there unless DELETE is supplied."
  (let* ((path (expand-file-name name extra-data-directory))
         (file (make-temp-file path nil extension)))
    (when delete (delete-file file))
    file))

(defun insert-org-file-link (file description)
  (require 'ol)
  (let ((rel (file-relative-name file results-directory)))
    (insert (org-link-make-string (concat "file:" rel) description))))

(defun diff-graph (s1 s2 t1 t2 &optional negated-p)
  (let ((dfile (extra-file (concat t1 t2 "-diffgraph-") ".svg")))
    (with-temp-buffer
      (call-process difffolded-executable nil (list t nil) nil s1 s2)
      (apply #'call-process-region nil nil
             flamegraph-executable
             :delete (list (list :file dfile) nil) nil
             "--title"
             (concat (if negated-p "Negated ")
                     "Differential Flame Graph "
                     "(" t1 " - " t2 ")")
             (if negated-p '("--negate"))))
    dfile))

(defun generate-diff-graphs (stack-data1 stack-data2 name1 name2)
  (when (and stack-data1 stack-data2
             (file-exists-p stack-data1)
             (file-exists-p stack-data2))
    (let ((file1 (diff-graph stack-data1 stack-data2 name1 name2))
          (file2 (diff-graph stack-data2 stack-data1 name2 name1 :negated)))
      (insert-org-file-link file1 "Differential Graph")
      (insert "\n")
      (insert-org-file-link file2 "Negated Differential Graph")
      (insert "\n"))))

(defun generate-perf-extras (title perfdata)
  (when perfdata
    (let* ((oldbuf (current-buffer))
           (filebase (replace-regexp-in-string "[ /.]" "-" title))
           (stackfile (extra-file (concat filebase "-stack-samples-") ".out"))
           (flamefile (extra-file (concat filebase "-flamegraph-") ".svg"))
           (spname (pretty-file-name stackfile))
           (fpname (pretty-file-name flamefile))
           (backup-enable-predicate #'ignore))
      (with-temp-buffer
        (set-visited-file-name stackfile :noquery)
        (call-process perf-executable nil '(t nil)
                      nil "script" "-i" perfdata)
        (call-process-region nil nil
                             stackcollapse-perf-executable
                             :delete '(t nil))
        (when-let* ((filt (getenv "PERF_FILTER"))
                    (grepexe (executable-find "grep")))
          (call-process-region nil nil grepexe
                               :delete '(t nil) nil filt))
        (save-buffer)
        (message "Wrote collapsed perf stack samples for %s to %s" title spname)
        (with-current-buffer oldbuf
          (insert-org-file-link stackfile "Stack Samples")
          (insert "\n"))
        (call-process-region nil nil flamegraph-executable :delete
                             (list (list :file flamefile) nil) nil
                             "--title" (concat title " Flame Graph"))
        (when (file-exists-p flamefile)
          (message "Wrote flamegraph for %s to %s" title fpname)
          (with-current-buffer oldbuf
            (insert-org-file-link flamefile "Flame Graph")
            (insert "\n"))))
      stackfile)))

(defun perf-data-file ()
  (and extra-data-directory perf-executable
       (file-executable-p perf-executable)
       (extra-file "perf" ".data" :delete)))

(defun run-benchmark-process (title table-name exe process-command pdata)
  "Run benchmarks for emacs executable EXE.

TITLE is the headline of the org-mode heading, TABLE-NAME is the
#+NAME of the resulting table, and COMMAND-ARGS are passed to
EXE. The process's stderr output is echoed to stderr.

Returns a closure. Called with no args or nil, it calls
`accept-process-output' on the running process. Otherwise it
accepts all output from the process and its pipe, and returns a
vector of the perf output file (if any) and a string of the
org-mode heading for the benchmark results."
  (let* ((benchbuf (generate-new-buffer (concat " *" title " benchmark output*")))
         (reported-compiling nil)
         (filt (lambda (proc string)
                 (ignore proc)
                 ;; TODO
                 (if (string-match-p "Compiling\\|Loading" string)
                     (unless reported-compiling
                       (setq reported-compiling t)
                       (message "%s: Compiling and Loading benchmarks..." title))
                   (princ (concat title ": " string)
                          #'external-debugging-output))))
         (pipe (make-pipe-process
                :name "pipe"
                :noquery t
                :filter filt
                :sentinel #'ignore))
         (proc (make-process
                :noquery t
                :name title
                :stderr pipe
                :buffer benchbuf
                :command process-command
                :sentinel #'simple-proc-sentinel))
         result)
    (process-put proc :report-finished
                 (concat title ": benchmarks finished"))
    (lambda (&optional arg)
      (cond
        ((not arg) (accept-process-output proc))
        (result)
        (t (while (accept-process-output proc))
           (while (accept-process-output pipe))
           (unwind-protect
               (with-current-buffer benchbuf
                 (goto-char (point-min))
                 (insert "* " title "\n")
                 (let ((oldp (point))
                       stackdata)
                   (insert ":PROPERTIES:\n"
                           ":Executable: " (abbreviate-file-name exe) "\n"
                           ":END:\n")
                   (setq stackdata (generate-perf-extras title pdata))
                   (insert "#+NAME: " table-name)
                   (put-text-property oldp (point) 'dont-print-me t)
                   (insert "\n")
                   (setq result (vector (buffer-string) stackdata))))
             (kill-buffer benchbuf)
             (unless result (setq result :error))))))))

(defun filter-buffer-substring-dont-print (beg end delete)
  "Use as the value of `filter-buffer-substring-function' to omit
the contents of a buffer with a non-nil `dont-print-me' text
property."
  (ignore delete)
  (let ((sparts nil)
        (pmatch beg)
        (last-start (unless (get-text-property beg 'dont-print-me) beg)))
    (while (not (eq end (setq pmatch (next-single-property-change
                                      pmatch 'dont-print-me nil end))))
      (if (get-text-property pmatch 'dont-print-me)
          (when last-start
            (push (buffer-substring last-start pmatch) sparts)
            (setq last-start nil))
        (setq last-start pmatch)))
    (when last-start
      (push (buffer-substring last-start end) sparts))
    (apply #'concat (nreverse sparts))))

(defun report-bench-start (runs exes file)
  (message "Running %d benchmark iteration%s on %d executable%s
Results will be%s printed after all benchmarks have run"
           runs (if (= runs 1) "" "s")
           exes (if (= exes 1) "" "s")
           (if file (concat " written to " (pretty-file-name file) " and") "")))

(defun run-benches (output-filename exes-builds-ids)
  (unless exes-builds-ids (finish "Nothing to do"))
  (let* ((execount (length exes-builds-ids))
         (bench-dir (expand-file-name "benchmarks" elisp-benchmarks-directory))
         (benches (selected-benches bench-dir))
         (filter-buffer-substring-function
          #'filter-buffer-substring-dont-print)
         running-benches
         stack-data-files)
    (report-bench-start benchmark-runs execount output-filename)
    (setq running-benches
          (mapcar (pcase-lambda (`[,exe ,header ,name])
                    (let* ((tempfile (perf-data-file))
                           (bcommand (get-benchmark-command exe tempfile)))
                      (run-benchmark-process header name exe bcommand tempfile)))
                  exes-builds-ids))
    (condition-case e
        (while (memq t (mapcar #'funcall running-benches)))
      (error
       (signal (car e)
               (cons "While running benchmarks" (cdr e)))))
    (with-temp-buffer
      (insert "#+TITLE: Benchmark Results\n\n")
      (insert "#+PROPERTY: Benchmarks " (string-join benches " ") "\n")
      (insert (format "#+PROPERTY: Benchmark_iterations %d\n\n" benchmark-runs))
      (put-text-property (point-min) (point) 'dont-print-me t)
      (setq stack-data-files
            (mapcar (lambda (bench)
                      (pcase-let ((`[,string ,stackfile]
                                   (funcall bench :done)))
                        (insert string "\n")
                        stackfile))
                    running-benches))
      (when (= execount 2)
        (pcase-let*
            ((row-count (length benches))
             (table-string (get-table-skeleton benches))
             (`(,s1 ,s2) stack-data-files)
             (`([,_ ,title1 ,(app remote-table-ref nr)]
                [,_ ,title2 ,(app remote-table-ref fr)])
              exes-builds-ids))
          (insert (format "* Difference (%s - %s)\n" title1 title2))
          (let ((oldp (point)))
            (generate-diff-graphs s1 s2 title1 title2)
            (put-text-property oldp (point) 'dont-print-me t))
          (insert table-string "\n")
          (insert-row-formula row-count nr "-" fr "; %.2f")
          (insert (format "* Ratio (%s / %s)\n%s\n"
                          title1 title2 table-string))
          (insert-row-formula
           row-count
           (format percentage-table-formula-format nr fr))))
      (delay-mode-hooks
        (org-mode)
        (unwind-protect
            (condition-case e
                (org-table-recalculate-buffer-tables)
              (error (signal (car e)
                             (cons "While recalculating org tables" (cdr e)))))
          (indent-region (point-min) (point-max))
          (when output-filename
            (write-file output-filename))
          (message "Results:\n\n%s"
                   (filter-buffer-substring (point-min) (point-max))))))))

(defun name-to-org-table-id (name)
  (concat (replace-regexp-in-string
           "\\([[:space:]]\\)\\|\\([([{]\\)\\|\\([])}]\\)"
           (lambda (str)
             (cond
               ((match-beginning 1) "_")
               ((match-beginning 2) "<")
               ((match-beginning 3) ">")
               (t str)))
           name)
          "_table"))

(defun read-builds ()
  "Read paths of emacs executables and ask to name them.
If there are valid ones in `command-line-args-left', just ask for
their names, otherwise interactively prompt for executable
paths."
  (let* ((argv-exes
          (delq nil (mapcar (lambda (f)
                              (if (file-executable-p f)
                                  f
                                (message "%s is not executable, ignoring" f)
                                nil))
                            argv)))
         (had-argv argv-exes)
         b bs-names)
    (while (if (not argv-exes)
               (unless had-argv
                 (when (or (not bs-names)
                           (y-or-n-p "Add another executable? "))
                   (setq b (read-string "Executable: "))
                   (not (string-empty-p b))))
             (setq b (pop argv-exes))
             t)
      (when (string= b "emacs")
        (setq b (expand-file-name invocation-name invocation-directory)))
      (if (not (file-executable-p b))
          (message "%s is not executable, try again" (pretty-file-name b))
        (let* ((tn (string-trim
                    (read-string
                     (format "Name for build at %s: " (pretty-file-name b)))))
               (id (name-to-org-table-id tn)))
          (push (vector b tn (concat id "_table")) bs-names))))
    (nreverse bs-names)))

(defun compare-builds ()
  "Handle command line args and start the benchmarks."
  (when (member "-h" argv) (finish help-message))
  (let* (outfile)
    (setq argv (copy-sequence argv))
    (when (member "-debug" argv)
      (setq debug-on-error t)
      (setq argv (delete "-debug" argv)))
    (when (member "-no-extra" argv)
      (setq perf-executable nil)
      (setq argv (delete "-no-extra" argv)))
    (while (pcase argv
             (`("-o" ,a . ,m)
              (setq outfile a)
              (setq argv m))
             (`("-runs" ,a . ,m)
              (if (string-match-p "\\`[[:digit:]]+\\'" a)
                  (setq benchmark-runs (string-to-number a))
                (warn "Argument %s to -runs is not an integer\nUsing %s"
                      a benchmark-runs))
              (setq argv m))
             (`("-s" ,s . ,m)
              (setq benchmark-selector s)
              (setq argv m))
             (`("-extra-dir" ,d . ,m)
              (setq extra-data-directory d)
              (setq argv m))))
    (when outfile
      (unless (string-suffix-p ".org" outfile)
        (setq outfile (concat outfile ".org")))
      (if (and (file-exists-p outfile)
               (not (y-or-n-p (concat outfile " exists, overwrite contents? "))))
          (setq outfile nil)
        (setq outfile (expand-file-name outfile))
        (setq results-directory (file-name-directory outfile))
        (make-directory results-directory :parents-too)))
    (when perf-executable
      (if extra-data-directory
          (make-directory extra-data-directory :parents-too)
        (setq extra-data-directory results-directory)))
    (run-benches outfile (read-builds))))

(defun main ()
  (condition-case err
      (progn
        (compare-builds)
        (message "Done"))
    (finished (when (cdr err) (apply #'message (cdr err)))))
  (kill-emacs 0))

(main)
