#!/usr/bin/env python
import os.path as osp
def get_srcdir():
    filename = osp.normcase(osp.dirname(osp.abspath(__file__)))
    return osp.realpath(filename)

def set_paths(name, subdir, basename):
    return name,  osp.join(data_path, subdir, basename + ".collapsed")

scripts_path = get_srcdir()
tools_root = osp.normpath(osp.join(scripts_path, ".."))
data_path = osp.normpath(osp.join(tools_root, "data"))


# MODIFY THIS
paths = (
    # set_paths("baseline1",  "baseline", "perf-inclist-x1-1593129541"),
    # set_paths("baseline2",  "baseline", "perf-inclist-x1-1593129542"),
    set_paths("baseline",  "baseline", "perf--x2-1593135920"),
    set_paths("bytecode-in-traceback",  "bytecode-in-traceback", "perf--x2-1593135919"),
)

MIN_SIGNIFICANT_PERCENT = 5.0

# Do we want backtrace stack entries or just the last function of the
# backtrace?
# For example: emacs;Ffuncall;Fsetcar 1763
#          vs: Fsetcar 1763
function_only = True

def abs_percent(a: int, b: int) -> float:
    if a > b:
        (a, b) = (b, a)
    return ((b - a) * 100) / b

def percent_str(a: int, b: int) -> str:
    signum = "-" if a > b else ""

    return f"{signum}%3.1f%%" % abs_percent(a, b)

def process_file(path: str) -> tuple:

    data = {}
    line_number: int = 0
    total_ticks = None

    def errmsg(msg: str):
        return f"Line {line_number}: {line}\n** {msg} **"

    for line in open(path, "r").readlines():
        line_number += 1
        fields = line.split()
        assert fields[0].startswith("emacs"), errmsg(
            "callbacks should start with 'emacs'"
        )
        try:
            ticks = int(fields[1])
        except:
            assert False, errmsg("second field expected to be a integer count")

        assert len(fields) == 2, errmsg(f"Line should have 2 fields; got {len(fields)}")

        if total_ticks is None:
            total_ticks = ticks

        entry = fields[0].split(";")[-1] if function_only else fields[0]
        data[entry] = data.get(entry, 0) + ticks
        pass
    return total_ticks, data

total_ticks0, data0 = process_file(paths[0][1])
total_ticks1, data1 = process_file(paths[1][1])
not_in_baseline = {}
diff = abs(total_ticks0 - total_ticks1)
print(f"{paths[0][0]} at: {total_ticks0} vs {paths[1][0]} at: {total_ticks1} {percent_str(total_ticks0, total_ticks1)}")

data1_keys = data1.keys()
for cs, ticks0 in data0.items():
    if cs in data1_keys:
        ticks1 = data1[cs]
        if abs_percent(ticks0, ticks1) > MIN_SIGNIFICANT_PERCENT:
            print(f"{cs}: {percent_str(ticks0, ticks1)}")
            pass
        pass
    pass
