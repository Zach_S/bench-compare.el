#!/bin/bash

# Requirements:
# * have perf-tools installed

# Get absolute path
cd $(dirname ${BASH_SOURCE[0]})
mydir=$(pwd)
echo $mydir

# ZACH_CODE_DIR=..
# PERF=${PERF:-"perf record"}
ITERATIONS=${ITERATIONS:=2}
# SAVANNAH_CODE_DIR=${SAVANNAH_CODE_DIR:-/src/external-vcs/gitlab/zach-emacs-baseline}
USER=${USER:-rocky}
HOME="/home/$USER"

cd $ZACH_CODE_DIR
PERF_FILE=zach-emacs.perf
# git status | head -1
# time /src/external-vcs/gitlab/zach-emacs-baseline/src/emacs -batch -l ${HOME}/.emacs.d/elpa/elisp-benchmarks-1.6/elisp-benchmarks.elc --eval "(elisp-benchmarks-run \"bubble[^-]\" nil ${ITERATIONS})" > /tmp/perl-orig.out &
# time /src/external-vcs/gitlab/zach-emacs/src/emacs -batch -l ${HOME}/.emacs.d/elpa/elisp-benchmarks-1.6/elisp-benchmarks.elc --eval "(elisp-benchmarks-run \"bubble[^-]\" nil ${ITERATIONS})" > /tmp/perf-modified.out
time /src/external-vcs/gitlab/zach-emacs-baseline/src/emacs -batch -l ${HOME}/.emacs.d/elpa/elisp-benchmarks-1.6/elisp-benchmarks.elc --eval "(elisp-benchmarks-run nil ${ITERATIONS})" > /tmp/perf-orig.out 2>&1 &
time /src/external-vcs/gitlab/zach-emacs/src/emacs -batch -l ${HOME}/.emacs.d/elpa/elisp-benchmarks-1.6/elisp-benchmarks.elc --eval "(elisp-benchmarks-run nil ${ITERATIONS})" >/tmp/perf-modified.out 2>&1
