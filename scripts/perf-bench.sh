#!/bin/bash

# Requirements:
# * perf-tools installed (Unix & GNU/Linux
# * Perl for flamegraph tools

# Get absolute path
cd $(dirname ${BASH_SOURCE[0]})
SCRIPT_DIR=$(pwd)

echo $SCRIPT_DIR

cd ${SCRIPT_DIR}/..
TOOLS_DIR=$(pwd)
BENCHMARK_DIR=${TOOLS_DIR}/elisp-benchmarks-1.6
DATA_DIR_BASE=${TOOLS_DIR}/data

PERF=${PERF:-"perf record -g"}

# TEST_EXPR='inclist'
TEST_EXPR=""

title="Zach bytecode in traceback vs reduced"
typeset -a name_pairs=("zach|/src/external-vcs/savannah/emacs" "reduced|/src/external-vcs/gitlab/zach-emacs-reduced/")
# typeset -a name_pairs=("baseline|../zach-emacs-baseline" "baseline|../zach-emacs-baseline")

ITERATIONS=${ITERATIONS:=2}
COMBINED_LOG=${DATA_DIR_BASE}/${name}/${TEST_EXPR}-x${ITERATIONS}-$(date "+%s").log
echo $title | tee $COMBINED_LOG
typeset -i n=0
typeset -a data_files=()
typeset -a folded_files=()
typeset -a collapsed_files=()
for name_pair in "${name_pairs[@]}"; do
    echo ==== $(date) === >> $COMBINED_LOG
    name="${name_pair%%|*}"
    dir="${name_pair#*|}"
    echo "$name ${TEST_EXPR}" >> $COMBINED_LOG
    cd $dir
    echo $(basename $(pwd)) | tee -a $COMBINED_LOG
    timestamp=$(date "+%s")
    git status | head -1 | tee -a $COMBINED_LOG
    basename=${DATA_DIR_BASE}/${name}/perf-${TEST_EXPR}-x${ITERATIONS}-${timestamp}
    data_file=${basename}.data
    data_files+=("$data_file")
    folded_file=${basename}.folded
    folded_files+=("$folded_file")
    collapsed_file=${basename}.collapsed
    collapsed_files+=("$collapsed_file")
    echo $outfile >> $COMBINED_LOG
    time $PERF \
	 -o $data_file \
	 ./src/emacs -batch -l \
	 ${TOOLS_DIR}/elisp-benchmarks-1.6/elisp-benchmarks.elc \
	 --eval "(elisp-benchmarks-run \"$TEST_EXPR\" nil ${ITERATIONS})" | tee -a $COMBINED_LOG 2>&1 &
    ((n+=1))
    sleep 1.0
done
wait
echo ==== $(date) === >> $COMBINED_LOG
for ((i=0; i<n; i++)); do
    perf script -i ${data_files[$i]} > ${folded_files[$i]}
    perl ${SCRIPT_DIR}/stackcollapse-perf.pl ${folded_files[$i]} > ${collapsed_files[$i]}
done
