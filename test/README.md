In this directory we have stuff to test our changes.

`test-debugger.elc` is compiled bytecode that when run should show the additional offsets information in the traceback.
`test-debugger.el` is the Elisp code for `test-debugger.elc`

To see the new information `M-x load-file test-debugger.elc`,

```
(eval-expression (progn (setq debug-on-error t) (test-debugger)))
```


and here is what the output looks like in `*Backtrace*`:

```
Debugger entered--Lisp error: (void-function test-debugger)
       (test-debugger)
       (progn (setq debug-on-error t) (test-debugger))
    27 (eval-expression (progn (setq debug-on-error t) (test-debugger)))
       eval((eval-expression (progn (setq debug-on-error t) (test-debugger))) nil)
    27 elisp--eval-last-sexp(nil)
    12 eval-last-sexp(nil)
       funcall-interactively(eval-last-sexp nil)
       call-interactively(eval-last-sexp nil nil)
   101 command-execute(eval-last-sexp)
```
