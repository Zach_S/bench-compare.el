# 2020 GSoC GNU Emacs project

A collection work for the 2020 GNU Emacs SoC project

## Analyzing performance

* `scripts` contains scripts for collecting and analyzing data
* `elisp-benchmarks-1.6` contains Andrea Corallo's benchmark suite
* `elisp-benchmarks.el` is modified to work with `scripts/compare-benches.el`
* `elisp-benchmarks-orig.el` is the original

`scripts/compare-benches.el` can be used run `elisp-benchmarks` against two built emacs versions.

Here is an example:


```
$ cd compare-benches.el/scripts

./compare-benches.el -runs 2 -s bubble /tmp/emacs{1,2}/src/emacs
Loading /etc/emacs/site-start.d/00debian.el (source)...
....
Name for build at /tmp/emacs1/src/emacs: zach offsets
Name for build at /tmp/emacs2/src/emacs: andrea remove indirection
Running 2 benchmark iterations on 2 executables
Results will be printed after all benchmarks have run
zach offsets: Compiling and Loading benchmarks...
andrea remove indirection: Compiling and Loading benchmarks...
zach offsets: Iteration number: 1
zach offsets: Running bubble-no-cons...
andrea remove indirection: Iteration number: 1
andrea remove indirection: Running bubble-no-cons...

## Source-code mappings

This directory contains work track source-code offsets in compilation

## test

This directory contains test programs to show off the work we've done.
